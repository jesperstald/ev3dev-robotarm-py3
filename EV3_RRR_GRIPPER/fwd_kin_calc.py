import sys
from math import *
sys.path.append("../")

# define forward kinematics
from RobotTools import *

q1, q2, q3 = sp.symbols("q1 q2 q3")
d = [17.5 * 8, 0, 0]
a = [0, 10 * 8, 23.5 * 8]
alpha = [-sp.pi / 2, 0, 0]
theta = [q1, q2, q3]
(fw_kin, T0_n) = forward_kin(d, a, alpha, theta, (q1, q2, q3))

print(T0_n.evalf())
