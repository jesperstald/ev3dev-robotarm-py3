import sympy as sp
import numpy as np


def rot_x(th, at=sp):
    if isinstance(th, str):
        th = sp.symbols(th)
    matr = [[1, 0, 0],
            [0, at.cos(th), -at.sin(th)],
            [0, at.sin(th), at.cos(th)]]
    return get_sp_or_np_matrix(matr, at)


def rot_y(th, at=sp):
    if isinstance(th, str):
        th = sp.symbols(th)
    matr = [[at.cos(th), 0, at.sin(th)],
            [0, 1, 0],
            [-at.sin(th), 0, at.cos(th)]]
    return get_sp_or_np_matrix(matr, at)


def rot_z(th, at=sp):
    if isinstance(th, str):
        th = at.symbols(th)
    matr = [[at.cos(th), at.sin(th), 0],
            [-at.sin(th), at.cos(th), 0],
            [0, 0, 1]]
    return get_sp_or_np_matrix(matr, at)


def get_sp_or_np_matrix(m, at):
    if at == sp:
        return sp.Matrix(m)
    elif at == np:
        return np.array(m)


def H(rot=None, dis=None, precision=1e-14):
    # return homogeneous matrix
    if rot is None:
        rot = sp.eye(3)
    if dis is None:
        dis = sp.Matrix([0, 0, 0])

    h = dis.col_insert(0, rot)
    return sp.Matrix([0, 0, 0, 1]).T.row_insert(0, h)


def forward_kin(d, a, alpha, theta, qs):
    # return forward kinematics sp.matrix from DH-parameters
    T0_ = []
    T0_n = sp.eye(4)
    for n in range(len(d)):
        A1 = H(rot_z(theta[n]))
        A2 = H(dis=sp.Matrix([0, 0, d[n]]))
        A3 = H(dis=sp.Matrix([a[n], 0, 0]))
        A4 = H(rot_x(alpha[n]))
        T0_.append(A1 * A2 * A3 * A4)
        T0_n *= T0_[n]

    return (sp.lambdify(qs, T0_n, "numpy"), T0_n)


def rolling_constraint(alpha, beta, l, wheeltype="fixed"):
    if wheeltype is "fixed" or "steered":
        return sp.Matrix([sp.sin(alpha + beta), -sp.cos(alpha + beta), -l * sp.cos(beta)])


def sliding_constraint(alpha, beta, l, wheeltype="fixed"):
    if wheeltype is "fixed" or "steered":
        return sp.Matrix([sp.cos(alpha + beta), sp.sin(alpha + beta), l * sp.sin(beta)])


def wrap2pi(phases):
    if phases == np.pi:
        return np.pi
    else:
        return (phases + np.pi) % (2 * np.pi) - np.pi
