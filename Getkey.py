import tty
import sys
import termios


def getkey(numkeys=1):
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(numkeys)
        # print(ch)

    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)

    return ch
