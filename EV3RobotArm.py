#!/usr/bin/env python3
from math import *
import numpy as np
import ev3dev2.motor as em

from RobotTools import *
# all units are assumed to be in degrees / mm !!


class Link:

    def __init__(self, motor_type, motor_addr, ratio, type="rot",
                 speed=None, mrange=None):
        motor_fcn = getattr(em, motor_type + "Motor")
        self.motor = motor_fcn(getattr(em, "OUTPUT_" + motor_addr))
        self.ratio = ratio
        self.type = type
        if speed is None:
            self.motor.speed_sp = self.motor.max_speed / 2
        else:
            self.motor.speed_sp = speed * ratio

        if mrange is None:
            mrange = [-inf, inf]
        self.mrange = mrange

    def move_abs(self, sp):
        self.ismovevalid(sp)
        self.motor.position_sp = sp * self.ratio
        self.motor.run_to_abs_pos()

    def move_rel(self, dist):
        self.move_abs(self.motor.position_sp / self.ratio + dist)

    def home(self):
        self.move_abs(0)

    def reset_pos(self, pos=0):
        self.motor.position = pos * self.ratio
        self.motor.position_sp = pos * self.ratio

    def get_pos(self):
        return self.motor.position / self.ratio

    def get_pos_rad(self):
        if self.type == "rot":
            return self.get_pos() * pi / 180
        return self.get_pos()

    def set_speed(self, speed):
        self.motor.speed_sp = speed * self.ratio

    def get_speed(self):
        return self.motor.speed_sp / self.ratio

    def get_max_speed(self):
        return self.motor.speed_max / self.ratio

    def ismovevalid(self, sp, mtime=None):
        if sp < self.mrange[0] or sp > self.mrange[1]:
            raise ValueError("Motor sp out out range!")

        if mtime is not None and (abs(self.get_pos() - sp) / mtime) > self.get_max_speed():
            raise ValueError("Speed SP too fast!")

        return True

    def __repr__(self):
        return "<Link-{}-{}-{}>".format(self.type, type(self.motor).__name__, self.motor.address)


class RobotArm:

    def __init__(self, links=None, init_pos=None,
                 forward_kin=None, rev_kin=None):
        if links is None:
            links = []
        self.links = links

        if init_pos is None:
            init_pos = [0 for _ in range(len(self))]
        self.init_pos = init_pos
        self.reset_pos()

        # should be a lambda function taking q1 q2 .. qn as input
        self.forward_kin = forward_kin
        # method handle, which returns list of q with H as input
        self.rev_kin = rev_kin

    def __getitem__(self, ix):
        return self.links[ix]

    def __len__(self):
        return len(self.links)

    def add_link(self, link, init_pos=0):
        self.links.append(link)
        # self.init_pos.append(init_pos)

    def move_links(self, positions, mtime=None):
        # check if move is valid
        for l, p in zip(self.links, positions):
            l.ismovevalid(p, mtime)

        for l, p in zip(self.links, positions):
            if mtime is not None:
                oldspeed = l.get_speed()
                l.set_speed(abs(l.get_pos() - p) / mtime)
                l.move_abs(p)
                l.set_speed(oldspeed)
            else:
                l.move_abs(p)

    def move(self, coord, mtime=None):
        pos = self.rev_kin(*tuple(coord))
        self.move_links(pos, mtime)

    def home(self, mtime=None):
        self.move_links(self.init_pos, mtime)

    def reset_pos(self):
        for l, p in zip(self.links, self.init_pos):
            l.reset_pos(p)

    def get_tool_pos(self):
        # returns the position of the tip of the last link interms of the
        # robots base workspace, in the format [x, y, z]
        if self.forward_kin is None:
            return np.array([0.0, 0.0, 0.0])
        qs = [l.get_pos_rad() for l in self.links]
        T = self.forward_kin(*tuple(qs))
        return T[0:3:1, -1]

    def calibrate(self):
        from Getkey import getkey
        link_ix = 0

        print("Calibrating link {}: {}".format(link_ix, self.links[link_ix]))
        while True:
            # get key - check if arrow (more chars)
            key = getkey(1)
            if key is "\x1b":
                key2 = getkey(2)

                if "[A" in key2:
                    self.links[link_ix].move_rel(1)

                elif "[B" in key2:
                    self.links[link_ix].move_rel(-1)

            elif key is "\r":
                link_ix += 1
                if link_ix < len(self.links):
                    print("Calibrating link {}: {}".format(link_ix, self.links[link_ix]))

                else:
                    break

        # calibration finished
        self.reset_pos()
        print("Calibration finished!")
