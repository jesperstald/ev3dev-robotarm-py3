import sys
from math import *

import numpy as np
import sympy as sp
import matplotlib.pyplot as plt

from RobotTools import *


class SMR:

    def __init__(self, pose=None, robotpar=None, ts=None, k=None):

        self.reset(pose)

        if robotpar is None:
            robotpar = np.array([0.26, 0.035, 0.035])
        self._robotpar = robotpar

        if ts is None:
            ts = 0.01
        self._ts = ts
        self._time = 0

        if k is None:
            k = np.array([0.3, 0.8, -0.15])
        self._k = k

        # define kinematics of SMR using sympy
        ws_r, ws_l, th = sp.symbols('ws_r ws_l th')
        dx, dy, dth = sp.symbols('dx dy dth')
        constraints_in = sp.Matrix([rolling_constraint(-pi / 2, pi, self.w() / 2).T,
                                    rolling_constraint(pi / 2, 0, self.w() / 2).T,
                                    sliding_constraint(pi / 2, 0, self.w() / 2).T])
        constraints_out = sp.Matrix([self.r_right() * ws_r, self.r_left() * ws_l, 0])

        rev_kin_eq = constraints_in * rot_z(th) * sp.Matrix([dx, dy, dth])
        self.rev_kin = sp.lambdify((dx, dy, dth, th), rev_kin_eq, "numpy")

        fwd_kin_eq = rot_z(th) ** -1 * constraints_in ** -1 * constraints_out
        self.fwd_kin = sp.lambdify((th, ws_r, ws_l), fwd_kin_eq, "numpy")

    def kinupdate(self, wheelspeed):

        # calculate speed and update pose
        speed = self.fwd_kin(self.theta(), wheelspeed[0], wheelspeed[1])
        new_pose = self._pose + speed[:, 0] * self._ts

        self._pose = new_pose
        # self._pose_his = np.append(self._pose_his, np.array([new_pose]), axis=0)
        self.log(new_pose)
        # print((self._pose))
        self._time += self._ts
        return self._pose

    def fwd(self, dist, speed):
        startpos = 1 * self._pose[0:2]
        ws_rad = self.ms2rads(speed)

        # determine driving direction
        if dist < 0:
            dist *= -1
            ws_rad *= -1

        # print("speed rpm:", ws_rpm)
        # print("speed:", ws_rad)
        while np.linalg.norm(self.curpos() - startpos) < dist:
            self.kinupdate(ws_rad)

    def turn(self, angle, speed):
        startpos = 1 * self._pose[2]
        ws_rad = self.ms2rads(speed)

        # determine turning direction
        if angle > 0:
            ws_rad[1] *= -1

        else:
            ws_rad[0] *= -1

        while np.abs(self._pose[2] - startpos) < np.abs(angle):
            self.kinupdate(ws_rad)

    def movetopose_simple(self, input_pose, speed):
        # determine angle to target and distance
        target_pose = self._pose + input_pose
        angle1 = np.arctan2(target_pose[1], target_pose[0])
        dist = np.linalg.norm(target_pose[0:2] - self.curpos())
        angle2 = target_pose[2]

        # move and turn
        self.turn(angle1, speed)
        self.fwd(dist, speed)
        self.turn(angle2, speed)

    def movetopose(self, input_pose, speed):
        # determine target pose and pose in target coordinate system
        target_pose = self._pose + input_pose
        R = rot_z(target_pose[2], np)
        n = 0
        n_max = 1e4
        speed_lim = 0.005

        # move!
        rho, alpha, beta = self.calc_target_pose(target_pose, R)
        while (rho > 0.01 or np.abs(self.theta() - target_pose[2]) > 0.02) and n < n_max:

            # calc speed and angular speed
            v = self._k[0] * rho
            # if v <= min_speed:
            #   v = 0
            omega = self._k[1] * alpha + self._k[2] * beta
            # if np.abs(omega) < min_speed:
            #   if omega > 0:
            #     omega = min_speed
            #   else:
            #     omega = -min_speed

            wheelspeed = v + np.array([1, -1]) * omega * self.w() / 2
            self.kinupdate(wheelspeed / np.array([self.r_right(), self.r_left()]))
            n += 1
            rho, alpha, beta = self.calc_target_pose(target_pose, R)

        self.cleanup()

    def calc_target_pose(self, target_pose, R):
        pose_t = np.matmul(R, (self._pose - target_pose))
        rho = np.linalg.norm(pose_t[0:2])

        # account for situation with only turning
        if rho != 0:
            alpha = wrap2pi(-pose_t[2] + np.arctan2(-pose_t[1], -pose_t[0]))
            beta = wrap2pi(-pose_t[2] - alpha)
        else:
            alpha = 0
            beta = wrap2pi(pose_t[2])

        return (rho, alpha, beta)

    def reset(self, pose=None):
        self._his_cap = 1
        self._his_size = 1
        self._pose_his = np.zeros([self._his_cap, 3])
        if pose is None:
            pose = np.zeros(3)
        self._pose = pose
        self._pose_his[0, :] = pose

    def log(self, row):
        # insert row in history - grow array when needed to make room.
        if self._his_size == self._his_cap:
            self._his_cap *= 4
            newdata = np.zeros((self._his_cap, 3))
            newdata[:self._his_size, :] = self._pose_his
            self._pose_his = newdata

        self._pose_his[self._his_size, :] = row
        self._his_size += 1

    def cleanup(self):
        self._his_cap = 1 * self._his_size
        data = self._pose_his[:self._his_cap, :]
        self._pose_his = data

    def w(self):
        return self._robotpar[0]

    def r_right(self):
        return self._robotpar[1]

    def r_left(self):
        return self._robotpar[2]

    def x(self):
        return self._pose[0]

    def y(self):
        return self._pose[1]

    def theta(self):
        return self._pose[2]

    def curpos(self):
        return self._pose[0:2]

    def ms2rads(self, speed):
        ws_rpm = speed / (2 * np.pi * np.array([self.r_right(), self.r_left()]))
        ws_rad = 2 * np.pi * ws_rpm
        return ws_rad

    # def plot(self, filename=):
    #   line = plt.Polygon(points, closed=None, fill=None, edgecolor='r')
