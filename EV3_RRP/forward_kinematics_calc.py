import sys
from math import *
sys.path.append("../")

# define forward kinematics
from RobotTools import *

q1, q2, q3 = sp.symbols("q1 q2 q3")
d1 = 17.5 * 8
d = [d1, 0, q3]
a = [0, 0, 0]
alpha = [-pi / 2, pi / 2, pi / 2]
theta = [q1, q2, pi]
(fw_kin, T0_n) = forward_kin(d, a, alpha, theta, (q1, q2, q3))

print(T0_n)
