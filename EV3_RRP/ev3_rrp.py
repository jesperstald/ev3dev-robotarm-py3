#! /usr/bin/python3
import time
import sys
from math import *

import numpy as np

sys.path.append("../")
from EV3RobotArm import *

# define gear ratioes for each link, such that motor.position / ratio equals:
# - the actual degrees that the link has rotated for rotational joints
# - the actual millimeters of actuation of the link for prismatic joints


# reverse kinematics - return in degrees!
def rev_kin(x, y, z):
    d1 = 16.5 * 8
    r = sqrt(x**2 + y**2)
    s = z - d1
    q = np.array([0.0, 0.0, 0.0])
    q[0] = atan2(y, x) * 180 / pi
    q[1] = (atan2(s, r) + pi / 2) * 180 / pi
    q[2] = sqrt(r**2 + s**2)
    return q


def forward_kin(q1, q2, q3):
    T = np.array([[-cos(q1) * cos(q2), sin(q2) * cos(q1), -sin(q1), q3 * sin(q2) * cos(q1)],
                  [-sin(q1) * cos(q2), sin(q1) * sin(q2), cos(q1), q3 * sin(q1) * sin(q2)],
                  [sin(q2), cos(q2), 0, q3 * cos(q2) + 132.0],
                  [0, 0, 0, 1]])
    return T

# # test forward kin
# T = forward_kin(0, float(pi / 2), 5.5 * 8)
# print(T)
# print(T[0:3:1, -1])


def sample_trajectory(r):
    # run problem 3 trajectory
    r.move([105, 40, 132], 3)
    time.sleep(3.5)
    r.move([110, -40, 132], 3)
    time.sleep(3.5)
    r.move([115, 40, 132], 3)
    time.sleep(3.5)
    r.move([120, -40, 132], 3)
    time.sleep(3.5)
    r.move([125, 40, 132], 3)
    time.sleep(4)
    r.home(5)
    time.sleep(5)


# create robot arm
def setup_robot():
    links = []
    links.append(Link("Large", "C", ratio=3, type="rot", speed=15,
                      mrange=[-90, 90]))
    links.append(Link("Large", "A", ratio=10, type="rot", speed=10,
                      mrange=[45, 135]))
    links.append(Link("Medium", "B", ratio=360 / (40 / 26), type="pris", speed=5,
                      mrange=[12.5 * 8, 17.5 * 8]))
    r = RobotArm(links, [0, 90, 12.5 * 8], forward_kin=forward_kin,
                 rev_kin=rev_kin)
    return r


if __name__ == "__main__":
    r = setup_robot()
    sample_trajectory(r)
